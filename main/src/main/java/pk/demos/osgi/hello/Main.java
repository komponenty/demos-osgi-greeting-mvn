package pk.demos.osgi.hello;


import java.util.HashMap;
import java.util.Map;
import org.apache.felix.framework.Felix;
import org.osgi.framework.*;

public class Main {
	static Felix framework;

	public static void main(String[] args) throws Exception {
		try {
			final Map<String, String> configMap = new HashMap<>();
			configMap.put(Constants.FRAMEWORK_STORAGE_CLEAN, "onFirstInit");
			framework = new Felix(configMap);
			framework.init();
			
			final BundleContext context = framework.getBundleContext();
			
			Bundle provider = context.installBundle("file:../bundles/provider/target/provider-3.0.jar");
			Bundle client = context.installBundle("file:../bundles/client/target/client-3.0.jar");
			
			framework.start();
			
			provider.start();
			client.start();
			client.stop();
			provider.stop();
			
			framework.stop();
		} catch (Exception ex) {
			System.err.println("Error starting program: " + ex);
			ex.printStackTrace();
			System.exit(0);
		}
	}
}
