package pk.demos.osgi.hello.provider;

public interface Greeting {

	public abstract void sayHello();

}